use std::sync::{Arc, RwLock};
use http::header::HeaderMap;
use tokio::sync::broadcast::{self, Sender, Receiver};
use mpart_async::server::ParseOutput;

pub type Payload = Arc<ParseOutput>;

#[derive(Clone)]
pub struct App {
    state: Arc<RwLock<Option<State>>>,
}

impl App {
    pub fn new() -> App {
        App {
            state: Arc::new(RwLock::new(None)),
        }
    }

    pub fn source(&self, headers: Arc<HeaderMap>, boundary: Arc<Vec<u8>>) -> Sender<Payload> {
        let (tx, _rx) = broadcast::channel(64);
        let mut state = self.state.write().unwrap();
        *state = Some(State {
            headers,
            boundary,
            tx: tx.clone(),
        });

        tx
    }

    pub fn clear(&self) {
        let mut state = self.state.write().unwrap();
        *state = None;
    }

    pub fn subscribe(&self) -> Option<Receiver<Payload>> {
        let state = self.state.read().unwrap();
        state.as_ref().map(|state| {
            state.tx.subscribe()
        })
    }

    pub fn boundary(&self) -> Option<Arc<Vec<u8>>> {
        let state = self.state.read().unwrap();
        state.as_ref().map(|state| state.boundary.clone())
    }

    pub fn headers(&self) -> Option<Arc<HeaderMap>> {
        let state = self.state.read().unwrap();
        state.as_ref().map(|state| state.headers.clone())
    }
}

pub struct AppSource {
    app: App,
}

impl Drop for AppSource {
    fn drop(&mut self) {
        let mut state = self.app.state.write().unwrap();
        *state = None;
    }
}

struct State {
    headers: Arc<HeaderMap>,
    boundary: Arc<Vec<u8>>,
    tx: Sender<Payload>,
}
