use std::{env::args, process::exit};
use futures::prelude::future::FutureExt;

mod app;
mod client;
mod server;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let mut args = args();
    let progname = args.next().unwrap();
    let target = if let Some(target) = args.next() {
        target
    } else {
        println!("Usage: {} <http://stream.mjpeg>", progname);
        exit(255);
    };

    let app = app::App::new();
    let c = tokio::spawn(client::run(app.clone(), target));
    let s = tokio::spawn(server::run(app));
    futures::select! {
        _ = c.fuse() => panic!("client exit"),
        _ = s.fuse() => panic!("server exit"),
    };
}
